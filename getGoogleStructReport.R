source("./oracleConnect.R")

q <- paste(
  "
  select
    str.ACCOUNTID,
    str.CAMPAIGNID,
    str.ADGROUPID,
    str.ACCOUNTNAME,
    str.CAMPAIGN,
    str.ADGROUP
  from
    SEM.SM_SEM_GOOGLE_STRUC_MERGED str
  group by
    str.ACCOUNTID,
    str.CAMPAIGNID,
    str.ADGROUPID,
    str.ACCOUNTNAME,
    str.CAMPAIGN,
    str.ADGROUP
  "
)

#"
#select
#  ADGROUP,
#  ADGROUPID,
#  CAMPAIGNID,
#  AGSTATUS
#from
#  REPORTS.SM_SEM_STRUCT_GOOGLE_ADGRP
#"

getGoogleStructReport <- function() {
  oracleConnect(q)
}